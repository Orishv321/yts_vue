import Vue from "vue";
import VueRouter from "vue-router";
import Content from "../components/Content";
import MovieDetail from "../views/MovieDetail";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Content",
    component: Content,
  },
  {
    path: "/movieDetail",
    name: "MovieDetail",
    component: MovieDetail,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
