import axios from "axios";

const state = {
  movies_Top: {},
  message: {},
  error_message: {},
};
const getters = {
  allTopMovies: (state) => state.movies_Top,
  Top_movie_message: (state) => state.message,
  Top_movie_errorMsg: (state) => state.error_message,
};
const actions = {
  getTopMovies_All({ commit }) {
    alert();
    commit("get_Top_Movies_All");
  },
};
const mutations = {
  get_Movies_All: async (state) => {
    console.log("At moview Get");
    try {
      const movies = await axios.get(
        "https://yts.mx/api/v2/list_movies.json?minimum_rating=7",
      );
      console.log(movies);

      if (movies) {
        state.movies_Top = movies;
        state.message = "Success";
      } else {
        state.message = "Error";
      }
    } catch (err) {
      state.error_message = "Moview not loaded";
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
