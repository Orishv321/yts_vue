import axios from "axios";

const state = {
  movies_All: {},
  message: {},
  error_message: {},
};
const getters = {
  allMovie: (state) => state.movies_All,
  movie_message: (state) => state.message,
  movie_errorMsg: (state) => state.error_message,
};
const actions = {
  getAllMovies({ commit }) {
    commit("get_Movies_All");
  },
};
const mutations = {
  get_Movies_All: async (state) => {
    console.log("At moview Get");
    try {
      const movies = await axios.get(
        "https://yts.mx/api/v2/list_movies.json?limit=30",
      );
      console.log(movies.data.data.movies);

      if (movies) {
        state.movies_All = movies.data.data.movies;
        state.message = "Success";
      } else {
        state.message = "Error";
      }
    } catch (err) {
      state.error_message = "Moview not loaded";
    }
  },
};
export default {
  state,
  getters,
  actions,
  mutations,
};
