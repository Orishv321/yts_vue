import Vue from "vue";
import Vuex from "vuex";
import allMovies from "./model/allMovies";
import topMovies from "./model/topMovies";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    allMovies,
    topMovies,
  },
});
